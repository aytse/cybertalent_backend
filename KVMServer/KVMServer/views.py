from django.conf import settings
from django.http import HttpResponse
from rest_framework.renderers import JSONRenderer
from rest_framework.decorators import api_view

import subprocess

class JSONResponse(HttpResponse):
    """
    An HttpResponse that renders its content into JSON.
    """

    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)

@api_view(['GET'])
def api_start_instance(request, lab_name=None):
    # if authenticated

    print 'SUCCESSS'
    print lab_name
    try:
        output = subprocess.check_output('python /root/code/cybertalent_backend/main2.py %s %s' % (lab_name, settings.KVM_URL),
            stderr=subprocess.STDOUT,
            shell=True)
    except subprocess.CalledProcessError as mainexc:
        output = mainexc.output
        print "error code", mainexc.returncode, mainexc.output

    print output

    if not output:
        return JSONResponse('No scenario found', status=400)
    else:
        return JSONResponse(output, status=201)

@api_view(['POST'])
def api_clear_instance(request):
    # if authenticated

    print 'SUCCESSS'
    print request.data['sessionid']
    print request.data['labkeyname']
    
    sessionid = request.data['sessionid']
    labkeyname = request.data['labkeyname']

    try:
        # REPLACE WITH SCRIPT TO RUN WITH ARGUMENTS
        output = subprocess.check_output('python /root/code/cybertalent_backend/tear_down_modules.py %s %s' % (sessionid, labkeyname),
            stderr=subprocess.STDOUT,
            shell=True)
    except subprocess.CalledProcessError as mainexc:
        output = mainexc.output
        print "error code", mainexc.returncode, mainexc.output

    # POSS TODO: Output string check for errors
    print output

    if not output:
        return JSONResponse('No instance found', status=400)
    else:
        return JSONResponse(output, status=201)    
