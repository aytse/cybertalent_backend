import subprocess
import time
import sys

# this is the sessuin ID, which will be used to eleminate VMs associated with a user session
disk_location='/root/code/cybertalent_backend/disks/'
network_location='/root/code/cybertalent_backend/networks/'
config_location='/root/code/cybertalent_backend/config_files/'

#these two paramers are needed for this function (these are temporary global variables)
# save output of the list of networks, and then save intot an array 


# a modual to delete the disk 
def delete_disk(uuid, lab):

	cmd="virsh list --all|awk '{print $2}'|grep " + uuid

        print(cmd)

	#cmd = "ps -A|grep 'process_name'"
	print "VMs that are about to be eleminated via virt-manager"
	ps = subprocess.Popen(cmd,shell=True,stdout=subprocess.PIPE,stderr=subprocess.STDOUT)
	output = ps.communicate()[0]
	

	#put output into a list 
	vms_to_kill = output.splitlines()

	for vm in vms_to_kill:
		print "\n    Powering Down VM: " + vm
		subprocess.call(['virsh', 'shutdown', vm])

		# give time for vm to poweroff
		time.sleep(5)

		# undefine VM
		print "    Undefining VM:    " + vm
		subprocess.call(['virsh', 'undefine', vm])

		# get the absolute Path of the disk 
		full_disk_location = disk_location + lab + "/" + vm + ".qcow2"
		
		# delete disk 
		print "    Removing disk from location: " + full_disk_location
		subprocess.call(['rm', full_disk_location])


		# delete VM configuration file
		full_conf_location = config_location + lab + "/" + vm + ".xml"
		print "    Removing vm config file from location: " + full_conf_location
		subprocess.call(['rm', full_conf_location])

		
# a module to cleanup virtual bridges on kvm host nad network config XML files
def cleanup_networks(uuid):
	print "\n Now lets clean up the network portion of session " + uuid

	# grab the networks in whcih the session is activly using
	# NOTE: if user has multiple sessions open in different labs, this will delete all of them. 
	cmd="virsh net-list|grep -v default|awk '{print $1}'|grep " + uuid
	ps = subprocess.Popen(cmd,shell=True,stdout=subprocess.PIPE,stderr=subprocess.STDOUT)
	output = ps.communicate()[0]

	# put networks in a list 
	remove_network_config = output.splitlines()
	for network in remove_network_config:
		
		print "\n    Destoying network: " + network
		subprocess.call(['virsh', 'net-destroy', network])
		
		full_net_location = network_location + network + ".xml" 
		print "    Erasing network config file at: " + full_net_location
		subprocess.call(['rm', full_net_location])
		### rm full_net_location 


def main(uuid, lab):
    delete_disk(uuid, lab)
    cleanup_networks(uuid)	


###MAIN###		
# Load in the json file which contains lab disk/XML information 
if __name__ == '__main__':
	if len(sys.argv) != 3:
		print '@!@!@!@!'
	else:
		print '@!@!@!@!'+str(main(sys.argv[1], sys.argv[2]))

	# cleanup virbro

	# cleanup XML files 
	#/root/code/cybertalent_backend/networks/

# cleanup disks
#echo 'now we need to cleanup the disks'
###
