from django.http import HttpResponse
from rest_framework.renderers import JSONRenderer
from rest_framework.decorators import api_view
from serializers import VirtualMachineGet

from scenarios.models import Scenario

class JSONResponse(HttpResponse):
    """
    An HttpResponse that renders its content into JSON.
    """

    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)

@api_view(['GET'])
def api_vms(request, scenario=None):
    if request.method == 'GET':
        #if request.user.username != 'admin':
        #    return JSONResponse('Unauthorized user', status=400)
        #else:
        scenarios = Scenario.objects.all()
        if scenario is not None:
            scenarios = scenarios.filter(id=scenario)
        if len(scenarios) > 0:
            print scenarios
            vms = scenarios[0].get_vms()
            print vms
            serializer = VirtualMachineGet(list(vms.values()), many=True)
            return JSONResponse(serializer.data, status=201)
        else:
            return JSONResponse('No scenario found', status=400)
