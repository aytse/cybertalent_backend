from __future__ import unicode_literals


from django.db import models
from django.contrib.auth.models import User
from django.utils.encoding import python_2_unicode_compatible

# Create your models here.
@python_2_unicode_compatible
class Profile(models.Model):
    user = models.OneToOneField(User)

    class Meta:
        db_table = 'auth_profile'

    def __str__(self):
        return self.user.username
