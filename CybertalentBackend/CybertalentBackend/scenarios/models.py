from __future__ import unicode_literals

from django.db import models
from django.utils.encoding import python_2_unicode_compatible

@python_2_unicode_compatible
class Scenario(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.TextField(max_length=255)

    def __str__(self):
        return self.name

    def get_vms(self):
        vms = VirtualMachine.objects.filter(scenarios=self)
        return vms

@python_2_unicode_compatible
class VirtualMachine(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.TextField(max_length=255)
    scenarios = models.ForeignKey(Scenario, on_delete=models.CASCADE)
    url = models.TextField()

    def __str__(self):
        return self.name