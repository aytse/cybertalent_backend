from django.contrib import admin

from models import Scenario
from models import VirtualMachine

# Register your models here.
admin.site.register(Scenario)
admin.site.register(VirtualMachine)