from scenarios.models import VirtualMachine

from rest_framework import serializers

class VirtualMachineGet(serializers.ModelSerializer):
    class Meta:
        model = VirtualMachine
        fields = [
            'id', 'name', 'url'
        ]

    name = serializers.CharField(read_only=True)
    url = serializers.CharField(read_only=True)