import json
from pprint import pprint
import time
import uuid
import subprocess

import sys
import random
import string

root_disk_store = "/root/code/cybertalent_backend/disks/"
root_network_store = "/root/code/cybertalent_backend/networks/"
lab_manifest_location = "/root/code/cybertalent_backend/lab-manifest.json"

proper_name_list = []
url_list = []

# Gets port of running vm
def getPort(vm_name):
	#vm_name = "53201b60_base_lab01_vm01_centos"

	port = subprocess.check_output(['virsh', 'domdisplay', vm_name ])

	port = port.replace('spice://localhost:','')

	proxyPort = int(port)+60
	#print "\nVM name: " + vm_name + " is running on spice port " +  port + "\n"
	#print "\nVM name: " + vm_name + " is running on proxy port " +  str(proxyPort) + "\n"

	return proxyPort  # or [i, card]




#This function works great for hosts with one network interface, but not for more than one 
def collectParams(name, disk_location, XMLfile, network):
	
	#declare the words to replace in the XML file
	wordsTo_replace = {
		
		"@@@name@@@" : name,					#The name of the actual VM (not the file name)
		"@@@disk_location@@@" : disk_location, 	#The linked clone disk location
		"@@@network@@@" : network,			#The network to be declared	

	}

	#replace variables the words in xml file
	for key, value in wordsTo_replace.iteritems():

		updateFile(key, str(wordsTo_replace[key]), XMLfile)
		#print key, 'corresponds to', wordsTo_replace[key]

def replaceNetworks(XMLfile, network2, network3):
	
	#declare the words to replace in the XML file
	wordsTo_replace = {
		
		"@@@network2@@@" : network2,	#The name of the actual VM (not the file name)
		"@@@network3@@@" : network3 	#The linked clone disk location
	}

	#replace NETWORK 2 AND 3 variables the words in xml file
	for key, value in wordsTo_replace.iteritems():
		updateFile(key, str(wordsTo_replace[key]), XMLfile)

# function to take in a variable and print out the actual variable name
def updateFile(key, variable, XMLfile):
	
	#print "\nreplacing variable, " + key + " with variable " + variable + " in file, " + XMLfile + "\n"
	#print "\nEntering function to replace word: \n  " + key + " with value \"" + wordsTo_replace[key] + "\""
	# Read in the file
	with open(XMLfile, 'r') as file:
		filedata = file.read()

	# Replace the target string
	filedata = filedata.replace(key, str(variable))

	# Write the file out again
	with open(XMLfile, 'w') as file:
		file.write(filedata)

####DISK FUNCTIONS#### 	
def disk_creation(base_disk, labname, UID):
	
	# grepping to get name of the base VM 
	name_base_disk = base_disk.split('/')[int(len(base_disk.split('/'))) - 1]


	# variable to store location of new linked clone  
	linked_disk_location = root_disk_store + labname + "/" + UID + "_" + name_base_disk
		
	#print "\n       Copying linked clone of base disk \n\t\t" + base_disk + "\n\t to location: \n\t\t" + linked_disk_location
			
	# creates linked clone of base disk 
	subprocess.call(['qemu-img', 'create', '-f', 'qcow2', '-b', base_disk, linked_disk_location])
	return linked_disk_location

####VM CONF XML CREATION FUNCTIONS####
def xml_creation(base_xml, labname, UID, linked_disk_location, vm, base_network, network2, network3):
	 	
	# Get name of base XML file
	name_base_xml = base_xml.split('/')[int(len(base_xml.split('/'))) - 1]
	
	# append UID to the XML configuration file
	linked_xml_file_name = UID + "_" + name_base_xml
	
	# variable to hold the location of the newly appended UID file
	linked_xml_location = "/root/code/cybertalent_backend/config_files/lab01/" + linked_xml_file_name
			
	# remove some charaters to clean up variable
	vm_name = linked_xml_file_name.replace(".xml", "")
	
	# Creates copy of the base xml file with the appeneded UID 
	#print "\n       Copying base xml \n\t\t" + base_xml + "\n\t to location: \n\t\t" + linked_xml_location
	subprocess.call(['cp', base_xml, linked_xml_location])
	
	####SESSION NETWORK CREATION / INITIALIZATION#### 	
	unique_network = create_network(base_network, UID)

	# Enter vmname, disklocation, xml location, and base network into the VM's unique XML file (ONLY IF VM HAS ONE INTERFACE )
	#print "\n       Entering parameters in copied XML file to suit " + vm
	collectParams(vm_name, linked_disk_location, linked_xml_location, unique_network)


	# if both network 2 and 3 are False (0)
	if network2 != "0":
		#all hosts with one interface will pop in here 
		#print "three int"
		#funtion that populates network value
		network2 = UID + "_" + network2
		network3 = UID + "_" + network3	
		replaceNetworks(linked_xml_location, network2, network3)	


	

	#GO INTO THIS ONE IF HOST HAS EXACTLY 3 INTERFACES

	

	# Defining a virtual machine with the configuration XML file (create a virtual machine with the XML settings that we want)
	#print "\n       Defining " + vm + " with xml file..."
	subprocess.call(['virsh', 'define', linked_xml_location])		

	return vm_name

def create_network(base_network, UID):

	# append UID to the base network 
	unique_network = UID + "_" + base_network  
		
	#get full network path so that we can create a new file if needed 
	xml_unique_network = root_network_store + unique_network + ".xml"

	#create the network XML file if it does not already exist 
	try:
		file = open(xml_unique_network, 'r')
		#print "File, " + xml_unique_network +  " already exists, exiting function\n" 
	except IOError:
		file = open(xml_unique_network, 'w')
		#print "File does not exist, creating file, " + xml_unique_network 

		file.write("<network>\n")
		file.write(" <name>" + unique_network + "</name>\n")
		file.write(" <uuid></uuid>\n")
		file.write("</network>\n")
	     
		file.close() 


	# define/initialize a new network 
	subprocess.call(['virsh', 'net-define', xml_unique_network])
	
	subprocess.call(['virsh', 'net-start', unique_network])
	subprocess.call(['virsh', 'net-autostart', unique_network])


	
	# return name of the unique network
	return unique_network
	


# Important funciton 
def declare_vms(labname, data, UID, kvm_url):
	#print "\n " + labname + " selected..."
	number_vms = data[labname]["num_vms"]
        virtualMachines = []	


	#loop through all the vms in the lab and list out the stats 
	#print "Stats for " + labname + "\n"
	for x in range(0, int(number_vms)):
		vm = "vm" + str(x+1) 
		
		# get data from the JSON file 
		base_disk = data[labname][vm]["base_disk"]
		base_xml = data[labname][vm]["base_xml"]
		base_network = data[labname][vm]["network1"]
		network2 = data[labname][vm]["network2"]
		network3 = data[labname][vm]["network3"]
		proper_name = data[labname][vm]["name"]
		
		print "\n   " + vm + ":"
		#print "      Base Disk:" + base_disk
		#print "      Base XML:" + base_xml
		#print "      network1: " + base_network
		#print "      network2: " + network2
		#print "      network3: " + network3

		####DISK CREATION FUNCTION#### 	
		linked_disk_location=disk_creation(base_disk, labname, UID)
		
		####XML CONFIG FILE CREATION FUNCTION#### 	
		vm_name=xml_creation(base_xml, labname, UID, linked_disk_location, vm, base_network, network2, network3)

		

		# Start the virtual machine		
		#print "\n       Starting " + vm + "..."
		subprocess.call(['virsh', 'start', vm_name])

		# get the SPICE port on which the VM is running
		#print "HERE"+vm_name
		port = getPort(vm_name)

		# Prints an end statement
		#print("          " + '\x1b[6;30;42m' + 'Success!' + '\x1b[0m')

		# get url of vm 
		url = kvm_url+"/index.html?host=kvm&port=" + str(port)

		#print proper_name + ", " + url

		# add vmname and url to a list and return the lists 
		virtualMachines.append({"name": proper_name, "url": url})

	return virtualMachines


def main(lab, kvm_url):
	data = json.load(open(lab_manifest_location))

	UID = str(uuid.uuid4())
	#UID = str(UID.partition('-'))
	UID = UID.split('-')[0]
	ans=True

	#print "\n\nSession Unique ID is: " + UID
	#print """
	#================
	#1.Deploy lab01
	#2.Deploy lab02
	#3.Deploy lab03
	#4.Exit/Quit
	#================
	#"""
		
	virtualMachines = declare_vms(lab, data, UID, kvm_url)

	#print "\n\nDONE\n\n"

	#print "session ID: " + UID
	#print 'virtual machines: ' + virtualMachines

	output = {"sessionid": UID, "vms": str(virtualMachines)}

	return output


###MAIN###		
# Load in the json file which contains lab disk/XML information 
if __name__ == '__main__':
	if len(sys.argv) != 3:
		print '@!@!@!@!'
	else:
		print '@!@!@!@!'+str(main(sys.argv[1], sys.argv[2]))

#sessionID

#vm_name
#url (port)
